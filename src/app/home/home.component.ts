import { fake_tree } from './../fake_data/fake_data';
import {
  ISchema,
  IEList,
  EType,
  IConnection,
  ILinkList,
  ITarget,
  ISwitchList,
  InterfaceList,
  ITree,
} from './../fake_data/data_structure';
import { Component, OnInit } from '@angular/core';

import * as d3 from 'd3';
import { HierarchyPointLink } from 'd3';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  svg: any;
  g: any;
  schemaSectionWidth = '100%';
  schemaSectionHeight = '600';
  constructor() {}

  ngOnInit(): void {
    this.Initialize('div#schema');
  }

  Initialize(DomID: string): void {
    this.svg = d3
      .select(DomID)
      .append('svg')
      .attr('xmlsn', 'http://www.w3.org/2000/svg')
      .attr('width', this.schemaSectionWidth)
      .attr('height', this.schemaSectionHeight)
      .append('g')
      .attr('id', 'schema-group');

    this.ExeHierachy(fake_tree);
  }

  // RenderSwitch(switchMap: any): void {}
  ExeHierachy(treeData: ITree): void {
    const sTree = d3.hierarchy(treeData);
    const tree = d3.tree().nodeSize([10, 10])(sTree);
    console.log(tree);
    console.log(tree.links());
    // console.log(tree.descendants());
    // console.log(tree.ancestors());
    // console.log(tree.leaves());

    this.g = this.svg.append('g').attr('id', 'path');

    const node = this.g
      .append('g')
      .attr('class', 'type-group')
      .selectAll('.node')
      .data(tree.descendants())
      .enter()
      .append('rect')
      .attr('class', (d: any) => {
        return d.data.name;
      })
      .attr('display', (d: any) =>
        d.data.name === 'Root' ? 'none' : 'inherit'
      )
      .attr('width', 50)
      .attr('height', 50)
      .style('fill', '#0800ff')
      .attr('transform', (d: any) => {
        return `translate(${d.x + 450}, ${d.y * 10})`;
      })
      .exit()
      .remove();

    /* const lines = this.g
      .append('g')
      .attr('class', 'lines-group')
      .data(tree.links()) */
  }

  TranslateData(respData: ISchema) {}
}
