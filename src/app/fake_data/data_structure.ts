export interface ISchema {
  computeList: IEList[];
  switchList: ISwitchList[];
  resourceList: IEList[];
  connection: IConnection[];
}

export interface IEList {
  uuid: string;
  ip: string;
  mac: string;
  hostName: string;
  state: string;
  group: string;
  type: EType;
}

export enum EType {
  Gpurs = 'GPURS',
  PCIeSW = 'PCIeSW',
  System = 'System',
}

export interface IConnection {
  switch: string;
  linkList: ILinkList[];
}

export interface ILinkList {
  index: number;
  target: ITarget;
}

export interface ITarget {
  type: EType;
  uuid: string;
  ifindex: string;
}

export interface ISwitchList {
  management: boolean;
  uuid: string;
  ip: string;
  mac: string;
  totalPorts: number;
  type: EType;
  interface_list: InterfaceList[];
}

export interface InterfaceList {
  index: number;
  type: string;
  ifindex: string;
}

// Tree structure

export interface ITree {
  name: string;
  children?: ITree[];
}

// export interface XChild {
//   name: string;
//   children?: XChild[];
// }
