import { ITree } from './data_structure';

export const fake_schema = {
  computeList: [
    {
      uuid: 'sys-01',
      ip: '192.168.2.5',
      mac: '5e:1b:1a:cf:b1:31',
      hostName: 'localhost',
      state: 'Attached',
      group: '',
      type: 'System',
    },
  ],
  switchList: [
    {
      management: true,
      uuid: 'aXD',
      ip: '192.168.1.5',
      mac: 'b4:d7:98:8b:c4:55',
      totalPorts: 24,
      type: 'PCIeSW',
      interface_list: [
        {
          index: 1,
          type: '',
          ifindex: '1',
        },
        {
          index: 2,
          type: '',
          ifindex: '2',
        },
        {
          index: 2,
          type: '',
          ifindex: '2',
        },
        {
          index: 3,
          type: '',
          ifindex: '3',
        },
        {
          index: 4,
          type: '',
          ifindex: '4',
        },
        {
          index: 5,
          type: '',
          ifindex: '5',
        },
        {
          index: 6,
          type: '',
          ifindex: '6',
        },
        {
          index: 7,
          type: '',
          ifindex: '7',
        },
        {
          index: 8,
          type: '',
          ifindex: '8',
        },
        {
          index: 9,
          type: '',
          ifindex: '9',
        },
        {
          index: 10,
          type: '',
          ifindex: '10',
        },
        {
          index: 11,
          type: '',
          ifindex: '11',
        },
        {
          index: 12,
          type: '',
          ifindex: '12',
        },
        {
          index: 13,
          type: '',
          ifindex: '13',
        },
        {
          index: 14,
          type: '',
          ifindex: '14',
        },
        {
          index: 15,
          type: '',
          ifindex: '15',
        },
        {
          index: 16,
          type: '',
          ifindex: '16',
        },
        {
          index: 17,
          type: '',
          ifindex: '17',
        },
        {
          index: 18,
          type: '',
          ifindex: '18',
        },
        {
          index: 19,
          type: '',
          ifindex: '19',
        },
        {
          index: 20,
          type: '',
          ifindex: '20',
        },
        {
          index: 21,
          type: '',
          ifindex: '21',
        },
        {
          index: 22,
          type: '',
          ifindex: '22',
        },
        {
          index: 23,
          type: '',
          ifindex: '23',
        },
        {
          index: 24,
          type: '',
          ifindex: '24',
        },
      ],
    },
    {
      management: false,
      uuid: 'aXE',
      ip: '192.168.1.6',
      mac: 'b4:d7:98:8b:c4:66',
      totalPorts: 24,
      type: 'PCIeSW',
      interface_list: [
        {
          index: 1,
          type: '',
          ifindex: '1',
        },
        {
          index: 2,
          type: '',
          ifindex: '2',
        },
        {
          index: 2,
          type: '',
          ifindex: '2',
        },
        {
          index: 3,
          type: '',
          ifindex: '3',
        },
        {
          index: 4,
          type: '',
          ifindex: '4',
        },
        {
          index: 5,
          type: '',
          ifindex: '5',
        },
        {
          index: 6,
          type: '',
          ifindex: '6',
        },
        {
          index: 7,
          type: '',
          ifindex: '7',
        },
        {
          index: 8,
          type: '',
          ifindex: '8',
        },
        {
          index: 9,
          type: '',
          ifindex: '9',
        },
        {
          index: 10,
          type: '',
          ifindex: '10',
        },
        {
          index: 11,
          type: '',
          ifindex: '11',
        },
        {
          index: 12,
          type: '',
          ifindex: '12',
        },
        {
          index: 13,
          type: '',
          ifindex: '13',
        },
        {
          index: 14,
          type: '',
          ifindex: '14',
        },
        {
          index: 15,
          type: '',
          ifindex: '15',
        },
        {
          index: 16,
          type: '',
          ifindex: '16',
        },
        {
          index: 17,
          type: '',
          ifindex: '17',
        },
        {
          index: 18,
          type: '',
          ifindex: '18',
        },
        {
          index: 19,
          type: '',
          ifindex: '19',
        },
        {
          index: 20,
          type: '',
          ifindex: '20',
        },
        {
          index: 21,
          type: '',
          ifindex: '21',
        },
        {
          index: 22,
          type: '',
          ifindex: '22',
        },
        {
          index: 23,
          type: '',
          ifindex: '23',
        },
        {
          index: 24,
          type: '',
          ifindex: '24',
        },
      ],
    },
  ],
  resourceList: [
    {
      uuid: 'GPU-01',
      ip: '192.168.3.5',
      mac: 'ac:68:6d:51:49:5e',
      hostName: 'localhost',
      state: 'Attached',
      group: '',
      type: 'GPURS',
    },
  ],
  connection: [
    {
      switch: 'aXD',
      linkList: [
        {
          index: 1,
          target: {
            type: 'System',
            uuid: 'sys-01',
            ifindex: '',
          },
        },
        {
          index: 2,
          target: {
            type: 'System',
            uuid: 'sys-01',
            ifindex: '',
          },
        },
        {
          index: 3,
          target: {
            type: 'System',
            uuid: 'sys-01',
            ifindex: '',
          },
        },
        {
          index: 4,
          target: {
            type: 'System',
            uuid: 'sys-01',
            ifindex: '',
          },
        },
        {
          index: 17,
          target: {
            type: 'PCIeSW',
            uuid: 'aXE',
            ifindex: '1',
          },
        },
        {
          index: 18,
          target: {
            type: 'PCIeSW',
            uuid: 'aXE',
            ifindex: '2',
          },
        },
      ],
    },
    {
      switch: 'aXE',
      linkList: [
        {
          index: 1,
          target: {
            type: 'PCIeSW',
            uuid: 'aXD',
            ifindex: '17',
          },
        },
        {
          index: 2,
          target: {
            type: 'PCIeSW',
            uuid: 'aXD',
            ifindex: '18',
          },
        },
        {
          index: 9,
          target: {
            type: 'GPURS',
            uuid: 'GPU-01',
            ifindex: '',
          },
        },
        {
          index: 10,
          target: {
            type: 'GPURS',
            uuid: 'GPU-01',
            ifindex: '',
          },
        },
        {
          index: 11,
          target: {
            type: 'GPURS',
            uuid: 'GPU-01',
            ifindex: '',
          },
        },
        {
          index: 12,
          target: {
            type: 'GPURS',
            uuid: 'GPU-01',
            ifindex: '',
          },
        },
      ],
    },
  ],
};

export const fake_tree: ITree = {
  name: 'Root',
  children: [
    {
      name: 'Node1',
      children: [
        {
          name: 'Drive1',
        },
        {
          name: 'Drive1',
        },
        {
          name: 'Drive1',
        },
      ],
    },
    {
      name: 'Node1',
      children: [
        {
          name: 'Drive1',
        },
        {
          name: 'Drive1',
        },
      ],
    },
    {
      name: 'Node1',
      children: [
        {
          name: 'Drive1',
          children: [
            {
              name: 'Drive2',
            },
          ],
        },
      ],
    },
  ],
};
